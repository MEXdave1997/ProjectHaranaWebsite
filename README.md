# Project Harana Website Project

This is the repo for the Project Harana website.

The site itself is built with [Hugo](https://gohugo.io/), a static-site generator built in [Golang](https://golang.org/).
The Theme it uses is [hyde-hyde](https://github.com/htr3n/hyde-hyde).

## Building the Site
To build the site, make sure you have both Golang and Hugo installed (instructions can be found [here](https://golang.org/doc/install) and [here](https://gohugo.io/getting-started/installing/), respectively).

Then you want to pull down the repo with all the resources (including the theme submodule), using the following command:

```sh
git clone https://gitlab.com/MEXdave1997/ProjectHaranaWebsite --recurse-submodules
```

This will make sure that you have not only the content files, but the
corresponding theme (for developing purposes)
After that, with hugo installed, go into the project directory and start Hugo's development server:

```sh
cd [Project dir]
hugo server -D --gc
```
