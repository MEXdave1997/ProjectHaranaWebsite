---
title: "Meet The Crew: Adam!"
description: "Come and Meet Adam, one of the many male characters you meet in Carnations"
---

![Adam Sprite](/img/cast/Sprite-Adam-Bust.png)

Some basic facts about Adam:

* Height: 6ft 2in
* Race: Caucasian
* Country of Origin: Australia

**Personality:**

Adam is a veru soft-spoken guy, but can usually pull off a roast with a single
one-liner. He enjoys tabletop-games (think *Monopoly* and **Dungeons &
Dragons*). He works as a Teacher Assistant to pay his universtiy tuition.

**Voiced By**: 
