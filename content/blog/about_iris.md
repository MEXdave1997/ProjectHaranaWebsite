---
title: "Meet The Crew: Iris!"
description: "Come and Meet Iris, one of the many female characters you meet in Carnations"
---

![Iris Sprite](/img/cast/Sprite-Iris-Bust.png)

Some basic facts about Iris:

**Voiced By:** Anairis
