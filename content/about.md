---
title: "About Project Harana"
date: "2020-02-04"
---

# Who are Project Harana
Project Harana is a development studio with people from around the world.

# Team Members

* **Director & Founder** - Aika Intong

* **Programmers** 
  - Calix Romero
  - Nico
  - Kami

* **Artists**
  - Axi
  - Bun Nanami
  - Trail
  - Jia
  - OzumiiWizard
  - Visodette

* **Writers**
  - Roy
  - Aza Hudson
  - Sara

* **Music**
  - Felix Arifin
  - MJQP
  - Seluna
  - Soru

* **Video Editing**
  - Shiki

* **Website**
  - David Madrigal-Hernandez
